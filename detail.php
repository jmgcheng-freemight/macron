<?php 
	include 'header.php';
?>
	<main class="l-main ">
		<div class="l-content">
			<div class="l-gutter">

				

				<div class="l-main-content">

					<div class="page-breadcrumb">
						<p>
							<a href="#"><i class="icon-home"></i> Cagolab Design TPL</a> > <a href="#">xxxxxxxxxxxxxx</a> > <a href="#">xxxxxxxxxxxxxx</a> 
						</p>
					</div>


					<div class="product-fold">
						<div class="l-grid l-grid-2cols">
							<div class="l-grid-col">
								<div class="product-fold-gallery">
									
									<div class="slider slider-slick-gallery ">
										<div>
											<img src="img/product-square-1.jpg" />
										</div>
										<div>
											<img src="img/product-square-1.jpg" />
										</div>
										<div>
											<img src="img/product-square-1.jpg" />
										</div>
										<div>
											<img src="img/product-square-1.jpg" />
										</div>
										<div>
											<img src="img/product-square-1.jpg" />
										</div>
									</div>

									<div class="slider slider-slick-nav slider-slick-gallery-nav ">
										<div>
											<img src="img/product-mini-1.jpg" />
										</div>
										<div>
											<img src="img/product-mini-2.jpg" />
										</div>
										<div>
											<img src="img/product-mini-3.jpg" />
										</div>
										<div>
											<img src="img/product-mini-4.jpg" />
										</div>
										<div>
											<img src="img/product-mini-5.jpg" />
										</div>
									</div>


									<p>
										Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
										tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
										quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
										consequat.
									</p>
								</div>
							</div>
							<div class="l-grid-col">
								<div class="product-fold-desc">
									<form>
										<h2 class="product-fold-desc-h">
											xxxxxxxxxxxxxxxxxxxxxxxxxxx
											<span>xxxxxxx (xxxxx)</span>
										</h2>
										<p class="product-fold-desc-price">
											0,000X(XX)
										</p>
										<div class="product-fold-desc-meta">
											<p>
												xxxx: <span class="color-red">00pt</span> (xxxxxxxxxxxxxxxxxxxxxx)
											</p>
											<p>
												xxxxx: 3~4x
											</p>
										</div>
										<div class="product-fold-desc-meta">
											<p>
												<strong>Lorem ipsum dolor sit amet.</strong> consectetur adipisicing elit, sed do eiusmod
												tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
												quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
												consequat.
											</p>
										</div>
										<div class="product-fold-desc-options">
											<p>
												xxxx: 
												<select name="">
													<option value="">xxxxxx</option>
													<option value="">xxxxxx</option>
												</select>
											</p>
											<p>
												xxxx: 
												<select name="">
													<option value="">xxxxxx</option>
													<option value="">xxxxxx</option>
												</select>
											</p>
										</div>
										<hr />
										<p class="product-fold-desc-qty">
											xx: <input type="text" name="" value="" />
										</p>
										<div class="product-fold-desc-control">
											<div class="l-grid l-grid-2cols">
												<div class="l-grid-col align-left">
													<a class="button button-red button-cart" href="#"><i class="icon-cart align-middle"></i> xxxxx</a>
												</div>
												<div class="l-grid-col align-right">
													<a class="button button-white button-bordered button-favorite" href="#"><i class="icon-star align-middle"></i> xxxxxxxxxx</a>
												</div>
											</div>
										</div>
										<div class="product-fold-desc-share">
											share plugin
										</div>
									</form>
								</div>


								<div class="product-fold-control">
								</div>

							</div>
						</div>
					</div>


					<div class="product-detail">
						<header class="header-block">
							<h2 class="header-block-h">
								Detail
							</h2>
						</header>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. <br/>
							Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
						</p>
						<div class="product-detail-table">
							<div class="l-grid l-grid-2cols">
								<div class="l-grid-col">
									<div class="l-table width-full">
										<div class="l-table-row">
											<div class="l-table-cell">
												<div class="product-detail-table-label">
													<p>
														xxxx
													</p>
												</div>
											</div>
											<div class="l-table-cell width-full">
												<div class="product-detail-table-data">
													<p>
														xxxxxxxx
													</p>
												</div>
											</div>
										</div>
										<div class="l-table-row">
											<div class="l-table-cell">
												<div class="product-detail-table-label">
													<p>
														xxxx
													</p>
												</div>
											</div>
											<div class="l-table-cell width-full">
												<div class="product-detail-table-data">
													<p>
														xxxxxxxx
													</p>
												</div>
											</div>
										</div>
										<div class="l-table-row">
											<div class="l-table-cell">
												<div class="product-detail-table-label">
													<p>
														xxxx
													</p>
												</div>
											</div>
											<div class="l-table-cell width-full">
												<div class="product-detail-table-data">
													<p>
														xxxxxxxx
													</p>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="l-grid-col">
									<div class="l-table width-full">
										<div class="l-table-row">
											<div class="l-table-cell">
												<div class="product-detail-table-label">
													<p>
														xxxx
													</p>
												</div>
											</div>
											<div class="l-table-cell width-full">
												<div class="product-detail-table-data">
													<p>
														xxxxxxxx
													</p>
												</div>
											</div>
										</div>
										<div class="l-table-row">
											<div class="l-table-cell">
												<div class="product-detail-table-label">
													<p>
														xxxx
													</p>
												</div>
											</div>
											<div class="l-table-cell width-full">
												<div class="product-detail-table-data">
													<p>
														xxxxxxxx
													</p>
												</div>
											</div>
										</div>
										<div class="l-table-row">
											<div class="l-table-cell">
												<div class="product-detail-table-label">
													<p>
														xxxx
													</p>
												</div>
											</div>
											<div class="l-table-cell width-full">
												<div class="product-detail-table-data">
													<p>
														xxxxxxxx
													</p>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>


					<div class="product-review">
						<header class="header-block">
							<h2 class="header-block-h">
								Review
							</h2>
						</header>
						<div class="product-review-control">
							<div class="l-grid l-grid-2cols">
								<div class="l-grid-col align-left">
									<div class="product-review-count">
										<p>
											xxxx 2x
										</p>
									</div>
								</div>
								<div class="l-grid-col align-right">
									<a class="button button-darkgray button-font-normal" href="#"><i class="icon-pencil align-middle"></i> xxxxxxxxxxxxxxxxxxxxxxxx</a>
								</div>
							</div>
						</div>
						<div class="product-review-list">
							<ul>
								<li>
									<div class="product-review-list-head">
										<div class="l-table width-full">
											<div class="l-table-cell">
												<div class="product-review-list-head-stars">
													<i class="icon-star icon-gold align-middle"></i>
													<i class="icon-star icon-gold align-middle"></i>
													<i class="icon-star icon-gold align-middle"></i>
													<i class="icon-star icon-gold align-middle"></i>
													<i class="icon-star icon-gray align-middle"></i>
												</div>
											</div>
											<div class="l-table-cell width-full">
												<div class="product-review-list-head-title">
													<p>
														xxxxxxxxx!
													</p>
												</div>
											</div>
											<div class="l-table-cell align-right">
												<div class="product-review-list-head-meta">
													<p>
														2014/00/00 SAMPLE USERxx
													</p>
												</div>		
											</div>
										</div>
									</div>
									<div class="product-review-list-content">
										<p>
											Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
										</p>
									</div>
								</li>
								<li>
									<div class="product-review-list-head">
										<div class="l-table width-full">
											<div class="l-table-cell">
												<div class="product-review-list-head-stars">
													<i class="icon-star icon-gold align-middle"></i>
													<i class="icon-star icon-gold align-middle"></i>
													<i class="icon-star icon-gold align-middle"></i>
													<i class="icon-star icon-gold align-middle"></i>
													<i class="icon-star icon-gold align-middle"></i>
												</div>
											</div>
											<div class="l-table-cell width-full">
												<div class="product-review-list-head-title">
													<p>
														xxxxxxxxx!
													</p>
												</div>
											</div>
											<div class="l-table-cell align-right">
												<div class="product-review-list-head-meta">
													<p>
														2014/00/00 SAMPLE USERxx
													</p>
												</div>		
											</div>
										</div>
									</div>
									<div class="product-review-list-content">
										<p>
											Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
										</p>
									</div>
								</li>
							</ul>
						</div>
					</div>



					<div class="product-cards">
						<header class="header-block">
							<h2 class="header-block-h">
								Related Items
							</h2>
						</header>
						<ul class="l-grid l-grid-5cols">
							<li class="l-grid-col">
								<a class="product-cards-anc" href="detail.php">
									<div class="product-cards-portrait">
										<div class="product-cards-portrait-inner">
											<img src="img/product-1.jpg" />
										</div>
									</div>
									<div class="product-cards-detail">
										<h3 class="product-cards-name">xxxx</h3>
										<div class="product-cards-detail-meta">
											<p class="product-cards-detail-price">
												0,000X <span>(xx)</span>
											</p>
											<p class="product-cards-detail-summary">
												Lorem ipsum dolor sit amet, consectetur adipisicing.
											</p>
										</div>
									</div>
								</a>
							</li>
							<li class="l-grid-col">
								<a class="product-cards-anc" href="detail.php">
									<div class="product-cards-portrait">
										<div class="product-cards-portrait-inner">
											<img src="img/product-2.jpg" />
										</div>
									</div>
									<div class="product-cards-detail">
										<h3 class="product-cards-name">xxxx</h3>
										<div class="product-cards-detail-meta">
											<p class="product-cards-detail-price">
												0,000X <span>(xx)</span>
											</p>
											<p class="product-cards-detail-summary">
												Lorem ipsum dolor sit amet, consectetur adipisicing.
											</p>
										</div>
									</div>
								</a>
							</li>
							<li class="l-grid-col">
								<a class="product-cards-anc" href="detail.php">
									<div class="product-cards-portrait">
										<div class="product-cards-portrait-inner">
											<img src="img/product-3.jpg" />
										</div>
									</div>
									<div class="product-cards-detail">
										<h3 class="product-cards-name">xxxx</h3>
										<div class="product-cards-detail-meta">
											<p class="product-cards-detail-price">
												0,000X <span>(xx)</span>
											</p>
											<p class="product-cards-detail-summary">
												Lorem ipsum dolor sit amet, consectetur adipisicing.
											</p>
										</div>
									</div>
								</a>
							</li>
							<li class="l-grid-col">
								<a class="product-cards-anc" href="detail.php">
									<div class="product-cards-portrait">
										<div class="product-cards-portrait-inner">
											<img src="img/product-4.jpg" />
										</div>
									</div>
									<div class="product-cards-detail">
										<h3 class="product-cards-name">xxxx</h3>
										<div class="product-cards-detail-meta">
											<p class="product-cards-detail-price">
												0,000X <span>(xx)</span>
											</p>
											<p class="product-cards-detail-summary">
												Lorem ipsum dolor sit amet, consectetur adipisicing.
											</p>
										</div>
									</div>
								</a>
							</li>
							<li class="l-grid-col">
								<a class="product-cards-anc" href="detail.php">
									<div class="product-cards-portrait">
										<div class="product-cards-portrait-inner">
											<img src="img/product-5.jpg" />
										</div>
									</div>
									<div class="product-cards-detail">
										<h3 class="product-cards-name">xxxx</h3>
										<div class="product-cards-detail-meta">
											<p class="product-cards-detail-price">
												0,000X <span>(xx)</span>
											</p>
											<p class="product-cards-detail-summary">
												Lorem ipsum dolor sit amet, consectetur adipisicing.
											</p>
										</div>
									</div>
								</a>
							</li>
						</ul>
					</div>

				</div>




				

			</div>
		</div>
	</main>
<?php 
	include 'footer.php';
?>