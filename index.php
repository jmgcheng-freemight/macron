<?php 
	include 'header.php';
?>
	<main class="l-main ">
		<div class="l-content">
			<div class="l-gutter">

				<div class="l-main-content">
					<div class="slider slider-banner slider-slick">
						<div>
							<img src="img/banner-1.jpg" />
						</div>
						<div>
							<img src="img/banner-1.jpg" />
						</div>
						<div>
							<img src="img/banner-1.jpg" />
						</div>
					</div>

					<div class="compare-prod">
						<div class="l-grid l-grid-2cols">
							<div class="l-grid-col align-left">
								<img src="img/img-1.jpg" />
							</div>
							<div class="l-grid-col align-right">
								<img src="img/img-2.jpg" />
							</div>
						</div>
					</div>

					<div class="product-cards">
						<header class="header-block">
							<h2 class="header-block-h">
								New Items
							</h2>
						</header>
						<ul class="l-grid l-grid-5cols">
							<li class="l-grid-col product-cards-item">
								<a class="product-cards-anc" href="detail.php">
									<div class="product-cards-portrait">
										<div class="product-cards-portrait-inner">
											<img src="img/product-1.jpg" />
										</div>
									</div>
									<div class="product-cards-detail">
										<h3 class="product-cards-name">xxxx</h3>
										<div class="product-cards-detail-meta">
											<p class="product-cards-detail-price">
												0,000X <span>(xx)</span>
											</p>
											<p class="product-cards-detail-summary">
												xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx.
											</p>
										</div>
									</div>
								</a>
							</li>
							<li class="l-grid-col product-cards-item">
								<a class="product-cards-anc" href="detail.php">
									<div class="product-cards-portrait">
										<div class="product-cards-portrait-inner">
											<img src="img/product-2.jpg" />
										</div>
									</div>
									<div class="product-cards-detail">
										<h3 class="product-cards-name">xxxx</h3>
										<div class="product-cards-detail-meta">
											<p class="product-cards-detail-price">
												0,000X <span>(xx)</span>
											</p>
											<p class="product-cards-detail-summary">
												xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx.
											</p>
										</div>
									</div>
								</a>
							</li>
							<li class="l-grid-col product-cards-item">
								<a class="product-cards-anc" href="detail.php">
									<div class="product-cards-portrait">
										<div class="product-cards-portrait-inner">
											<img src="img/product-3.jpg" />
										</div>
									</div>
									<div class="product-cards-detail">
										<h3 class="product-cards-name">xxxx</h3>
										<div class="product-cards-detail-meta">
											<p class="product-cards-detail-price">
												0,000X <span>(xx)</span>
											</p>
											<p class="product-cards-detail-summary">
												xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx.
											</p>
										</div>
									</div>
								</a>
							</li>
							<li class="l-grid-col product-cards-item">
								<a class="product-cards-anc" href="detail.php">
									<div class="product-cards-portrait">
										<div class="product-cards-portrait-inner">
											<img src="img/product-4.jpg" />
										</div>
									</div>
									<div class="product-cards-detail">
										<h3 class="product-cards-name">xxxx</h3>
										<div class="product-cards-detail-meta">
											<p class="product-cards-detail-price">
												0,000X <span>(xx)</span>
											</p>
											<p class="product-cards-detail-summary">
												xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx.
											</p>
										</div>
									</div>
								</a>
							</li>
							<li class="l-grid-col product-cards-item">
								<a class="product-cards-anc" href="detail.php">
									<div class="product-cards-portrait">
										<div class="product-cards-portrait-inner">
											<img src="img/product-5.jpg" />
										</div>
									</div>
									<div class="product-cards-detail">
										<h3 class="product-cards-name">xxxx</h3>
										<div class="product-cards-detail-meta">
											<p class="product-cards-detail-price">
												0,000X <span>(xx)</span>
											</p>
											<p class="product-cards-detail-summary">
												xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx.
											</p>
										</div>
									</div>
								</a>
							</li>
						</ul>
					</div>


					<div class="product-cards">
						<header class="header-block">
							<h2 class="header-block-h">
								Ranking
							</h2>
						</header>
						<ul class="l-grid l-grid-5cols">
							<li class="l-grid-col product-cards-item">
								<a class="product-cards-anc" href="detail.php">
									<div class="product-cards-portrait">
										<div class="product-cards-portrait-rank gold">
											1
										</div>
										<div class="product-cards-portrait-inner">
											<img src="img/product-6.jpg" />
										</div>
									</div>
									<div class="product-cards-detail">
										<h3 class="product-cards-name">xxxx</h3>
										<div class="product-cards-detail-meta">
											<p class="product-cards-detail-price">
												0,000X <span>(xx)</span>
											</p>
											<p class="product-cards-detail-summary">
												xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx.
											</p>
										</div>
									</div>
								</a>
							</li>
							<li class="l-grid-col product-cards-item">
								<a class="product-cards-anc" href="detail.php">
									<div class="product-cards-portrait">
										<div class="product-cards-portrait-rank silver">
											2
										</div>
										<div class="product-cards-portrait-inner">
											<img src="img/product-7.jpg" />
										</div>
									</div>
									<div class="product-cards-detail">
										<h3 class="product-cards-name">xxxx</h3>
										<div class="product-cards-detail-meta">
											<p class="product-cards-detail-price">
												0,000X <span>(xx)</span>
											</p>
											<p class="product-cards-detail-summary">
												xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx.
											</p>
										</div>
									</div>
								</a>
							</li>
							<li class="l-grid-col product-cards-item">
								<a class="product-cards-anc" href="detail.php">
									<div class="product-cards-portrait">
										<div class="product-cards-portrait-rank bronze">
											3
										</div>
										<div class="product-cards-portrait-inner">
											<img src="img/product-8.jpg" />
										</div>
									</div>
									<div class="product-cards-detail">
										<h3 class="product-cards-name">xxxx</h3>
										<div class="product-cards-detail-meta">
											<p class="product-cards-detail-price">
												0,000X <span>(xx)</span>
											</p>
											<p class="product-cards-detail-summary">
												xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx.
											</p>
										</div>
									</div>
								</a>
							</li>
							<li class="l-grid-col product-cards-item">
								<a class="product-cards-anc" href="detail.php">
									<div class="product-cards-portrait">
										<div class="product-cards-portrait-rank">
											4
										</div>
										<div class="product-cards-portrait-inner">
											<img src="img/product-9.jpg" />
										</div>
									</div>
									<div class="product-cards-detail">
										<h3 class="product-cards-name">xxxx</h3>
										<div class="product-cards-detail-meta">
											<p class="product-cards-detail-price">
												0,000X <span>(xx)</span>
											</p>
											<p class="product-cards-detail-summary">
												xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx.
											</p>
										</div>
									</div>
								</a>
							</li>
							<li class="l-grid-col product-cards-item">
								<a class="product-cards-anc" href="detail.php">
									<div class="product-cards-portrait">
										<div class="product-cards-portrait-rank">
											5
										</div>
										<div class="product-cards-portrait-inner">
											<img src="img/product-10.jpg" />
										</div>
									</div>
									<div class="product-cards-detail">
										<h3 class="product-cards-name">xxxx</h3>
										<div class="product-cards-detail-meta">
											<p class="product-cards-detail-price">
												0,000X <span>(xx)</span>
											</p>
											<p class="product-cards-detail-summary">
												xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx.
											</p>
										</div>
									</div>
								</a>
							</li>
						</ul>
					</div>


					<div class="product-cards">
						<header class="header-block">
							<h2 class="header-block-h">
								Recommended Items
							</h2>
						</header>
						<ul class="l-grid l-grid-5cols">
							<li class="l-grid-col product-cards-item">
								<a class="product-cards-anc" href="detail.php">
									<div class="product-cards-portrait">
										<div class="product-cards-portrait-inner">
											<img src="img/product-11.jpg" />
										</div>
									</div>
									<div class="product-cards-detail">
										<h3 class="product-cards-name">xxxx</h3>
										<div class="product-cards-detail-meta">
											<p class="product-cards-detail-price">
												0,000X <span>(xx)</span>
											</p>
											<p class="product-cards-detail-summary">
												xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx.
											</p>
										</div>
									</div>
								</a>
							</li>
							<li class="l-grid-col product-cards-item">
								<a class="product-cards-anc" href="detail.php">
									<div class="product-cards-portrait">
										<div class="product-cards-portrait-inner">
											<img src="img/product-12.jpg" />
										</div>
									</div>
									<div class="product-cards-detail">
										<h3 class="product-cards-name">xxxx</h3>
										<div class="product-cards-detail-meta">
											<p class="product-cards-detail-price">
												0,000X <span>(xx)</span>
											</p>
											<p class="product-cards-detail-summary">
												xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx.
											</p>
										</div>
									</div>
								</a>
							</li>
							<li class="l-grid-col product-cards-item">
								<a class="product-cards-anc" href="detail.php">
									<div class="product-cards-portrait">
										<div class="product-cards-portrait-inner">
											<img src="img/product-13.jpg" />
										</div>
									</div>
									<div class="product-cards-detail">
										<h3 class="product-cards-name">xxxx</h3>
										<div class="product-cards-detail-meta">
											<p class="product-cards-detail-price">
												0,000X <span>(xx)</span>
											</p>
											<p class="product-cards-detail-summary">
												xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx.
											</p>
										</div>
									</div>
								</a>
							</li>
							<li class="l-grid-col product-cards-item">
								<a class="product-cards-anc" href="detail.php">
									<div class="product-cards-portrait">
										<div class="product-cards-portrait-inner">
											<img src="img/product-14.jpg" />
										</div>
									</div>
									<div class="product-cards-detail">
										<h3 class="product-cards-name">xxxx</h3>
										<div class="product-cards-detail-meta">
											<p class="product-cards-detail-price">
												0,000X <span>(xx)</span>
											</p>
											<p class="product-cards-detail-summary">
												xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx.
											</p>
										</div>
									</div>
								</a>
							</li>
							<li class="l-grid-col product-cards-item">
								<a class="product-cards-anc" href="detail.php">
									<div class="product-cards-portrait">
										<div class="product-cards-portrait-inner">
											<img src="img/product-15.jpg" />
										</div>
									</div>
									<div class="product-cards-detail">
										<h3 class="product-cards-name">xxxx</h3>
										<div class="product-cards-detail-meta">
											<p class="product-cards-detail-price">
												0,000X <span>(xx)</span>
											</p>
											<p class="product-cards-detail-summary">
												xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx.
											</p>
										</div>
									</div>
								</a>
							</li>
						</ul>
					</div>
				</div>








			</div>
		</div>
	</main>
<?php 
	include 'footer.php';
?>