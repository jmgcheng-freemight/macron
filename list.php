<?php 
	include 'header.php';
?>
	<main class="l-main l-main-is-aside-active">
		<div class="l-content">
			<div class="l-gutter">

				<?php 
					include 'aside.php';
				?>

				<div class="l-main-content">

					<div class="page-breadcrumb">
						<p>
							<a href="#"><i class="icon-home"></i> macron top</a> > <a href="#">xxx</a>
						</p>
					</div>

					<article class="article-archive">
						<header class="header-block">
							<h2 class="header-block-h">
								XXX
							</h2>
						</header>

						<div class="article-archive-hero">
							<img src="img/hero-1.jpg" />
						</div>

						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. <br/> 
							Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, 
							quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
							consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
							cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
							proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
						</p>

					</article>


					<div class="archive-grayblock">
						<h3 class="archive-grayblock-h">XXXX</h3>
						<nav>
							<ul class="l-grid l-grid-4cols">
								<li class="l-grid-col">
									<a href="#">
										XXXXXXX(X2XX)
									</a>
								</li>
								<li class="l-grid-col">
									<a href="#">
										XXXXXXX(X2XX)
									</a>
								</li>
								<li class="l-grid-col">
									<a href="#">
										XXXXXXX(X2XX)
									</a>
								</li>
								<li class="l-grid-col">
									<a href="#">
										XXXXXXX(X2XX)
									</a>
								</li>
							</ul>
						</nav>
					</div>

					
					<div class="product-cards product-cards-dashedbottom">

						<div class="product-cards-filter">
							<div class="l-grid l-grid-2cols">
								<div class="l-grid-col">
									<p class="product-cards-filter-ressum">
										XXXX <span class="color-red">72</span>X &nbsp;&nbsp; 
										XXXX &nbsp; <select name="">
														<option>16X</option>
													</select>
									</p>
								</div>
								<div class="l-grid-col align-right">
									<nav class="product-cards-filter-cat">
										<ul>
											<li>
												<a class="is-active" href="#">XXXXX</a>
											</li>
											<li>
												<a href="#">XXX</a>
											</li>
											<li>
												<a href="#">XXXXXX</a>
											</li>
											<li>
												<a href="#">XXXXX</a>
											</li>
										</ul>
									</nav>
								</div>
							</div>
						</div>

						<nav class="product-cards-paging">
							<ul>
								<li>
									<a href="#">&lt;</a>
								</li>
								<li>
									<a href="#">1</a>
								</li>
								<li>
									<a class="is-active" href="#">2</a>
								</li>
								<li>
									<a href="#">3</a>
								</li>
								<li>
									<a href="#">4</a>
								</li>
								<li>
									<a href="#">5</a>
								</li>
								<li>
									<a href="#">&gt;</a>
								</li>
							</ul>
						</nav>

						<hr />

						<ul class="l-grid l-grid-4cols">
							<li class="l-grid-col product-cards-item">
								<a class="product-cards-anc" href="detail.php">
									<div class="product-cards-portrait">
										<div class="product-cards-portrait-inner">
											<img src="img/product-1.jpg" />
										</div>
									</div>
									<div class="product-cards-detail">
										<h3 class="product-cards-name">xxxx</h3>
										<div class="product-cards-detail-meta">
											<p class="product-cards-detail-price">
												0,000X <span>(xx)</span>
											</p>
											<p class="product-cards-detail-summary">
												xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx.
											</p>
										</div>
									</div>
								</a>
							</li>
							<li class="l-grid-col product-cards-item">
								<a class="product-cards-anc" href="detail.php">
									<div class="product-cards-portrait">
										<div class="product-cards-portrait-inner">
											<img src="img/product-1.jpg" />
										</div>
									</div>
									<div class="product-cards-detail">
										<h3 class="product-cards-name">xxxx</h3>
										<div class="product-cards-detail-meta">
											<p class="product-cards-detail-price">
												0,000X <span>(xx)</span>
											</p>
											<p class="product-cards-detail-summary">
												xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx.
											</p>
										</div>
									</div>
								</a>
							</li>
							<li class="l-grid-col product-cards-item">
								<a class="product-cards-anc" href="detail.php">
									<div class="product-cards-portrait">
										<div class="product-cards-portrait-inner">
											<img src="img/product-1.jpg" />
										</div>
									</div>
									<div class="product-cards-detail">
										<h3 class="product-cards-name">xxxx</h3>
										<div class="product-cards-detail-meta">
											<p class="product-cards-detail-price">
												0,000X <span>(xx)</span>
											</p>
											<p class="product-cards-detail-summary">
												xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx.
											</p>
										</div>
									</div>
								</a>
							</li>
							<li class="l-grid-col product-cards-item">
								<a class="product-cards-anc" href="detail.php">
									<div class="product-cards-portrait">
										<div class="product-cards-portrait-inner">
											<img src="img/product-1.jpg" />
										</div>
									</div>
									<div class="product-cards-detail">
										<h3 class="product-cards-name">xxxx</h3>
										<div class="product-cards-detail-meta">
											<p class="product-cards-detail-price">
												0,000X <span>(xx)</span>
											</p>
											<p class="product-cards-detail-summary">
												xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx.
											</p>
										</div>
									</div>
								</a>
							</li>
							<li class="l-grid-col product-cards-item">
								<a class="product-cards-anc" href="detail.php">
									<div class="product-cards-portrait">
										<div class="product-cards-portrait-inner">
											<img src="img/product-1.jpg" />
										</div>
									</div>
									<div class="product-cards-detail">
										<h3 class="product-cards-name">xxxx</h3>
										<div class="product-cards-detail-meta">
											<p class="product-cards-detail-price">
												0,000X <span>(xx)</span>
											</p>
											<p class="product-cards-detail-summary">
												xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx.
											</p>
										</div>
									</div>
								</a>
							</li>
							<li class="l-grid-col product-cards-item">
								<a class="product-cards-anc" href="detail.php">
									<div class="product-cards-portrait">
										<div class="product-cards-portrait-inner">
											<img src="img/product-1.jpg" />
										</div>
									</div>
									<div class="product-cards-detail">
										<h3 class="product-cards-name">xxxx</h3>
										<div class="product-cards-detail-meta">
											<p class="product-cards-detail-price">
												0,000X <span>(xx)</span>
											</p>
											<p class="product-cards-detail-summary">
												xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx.
											</p>
										</div>
									</div>
								</a>
							</li>
							<li class="l-grid-col product-cards-item">
								<a class="product-cards-anc" href="detail.php">
									<div class="product-cards-portrait">
										<div class="product-cards-portrait-inner">
											<img src="img/product-1.jpg" />
										</div>
									</div>
									<div class="product-cards-detail">
										<h3 class="product-cards-name">xxxx</h3>
										<div class="product-cards-detail-meta">
											<p class="product-cards-detail-price">
												0,000X <span>(xx)</span>
											</p>
											<p class="product-cards-detail-summary">
												xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx.
											</p>
										</div>
									</div>
								</a>
							</li>
							<li class="l-grid-col product-cards-item">
								<a class="product-cards-anc" href="detail.php">
									<div class="product-cards-portrait">
										<div class="product-cards-portrait-inner">
											<img src="img/product-1.jpg" />
										</div>
									</div>
									<div class="product-cards-detail">
										<h3 class="product-cards-name">xxxx</h3>
										<div class="product-cards-detail-meta">
											<p class="product-cards-detail-price">
												0,000X <span>(xx)</span>
											</p>
											<p class="product-cards-detail-summary">
												xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx.
											</p>
										</div>
									</div>
								</a>
							</li>
							<li class="l-grid-col product-cards-item">
								<a class="product-cards-anc" href="detail.php">
									<div class="product-cards-portrait">
										<div class="product-cards-portrait-inner">
											<img src="img/product-1.jpg" />
										</div>
									</div>
									<div class="product-cards-detail">
										<h3 class="product-cards-name">xxxx</h3>
										<div class="product-cards-detail-meta">
											<p class="product-cards-detail-price">
												0,000X <span>(xx)</span>
											</p>
											<p class="product-cards-detail-summary">
												xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx.
											</p>
										</div>
									</div>
								</a>
							</li>
							<li class="l-grid-col product-cards-item">
								<a class="product-cards-anc" href="detail.php">
									<div class="product-cards-portrait">
										<div class="product-cards-portrait-inner">
											<img src="img/product-1.jpg" />
										</div>
									</div>
									<div class="product-cards-detail">
										<h3 class="product-cards-name">xxxx</h3>
										<div class="product-cards-detail-meta">
											<p class="product-cards-detail-price">
												0,000X <span>(xx)</span>
											</p>
											<p class="product-cards-detail-summary">
												xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx.
											</p>
										</div>
									</div>
								</a>
							</li>
							<li class="l-grid-col product-cards-item">
								<a class="product-cards-anc" href="detail.php">
									<div class="product-cards-portrait">
										<div class="product-cards-portrait-inner">
											<img src="img/product-1.jpg" />
										</div>
									</div>
									<div class="product-cards-detail">
										<h3 class="product-cards-name">xxxx</h3>
										<div class="product-cards-detail-meta">
											<p class="product-cards-detail-price">
												0,000X <span>(xx)</span>
											</p>
											<p class="product-cards-detail-summary">
												xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx.
											</p>
										</div>
									</div>
								</a>
							</li>
							<li class="l-grid-col product-cards-item">
								<a class="product-cards-anc" href="detail.php">
									<div class="product-cards-portrait">
										<div class="product-cards-portrait-inner">
											<img src="img/product-1.jpg" />
										</div>
									</div>
									<div class="product-cards-detail">
										<h3 class="product-cards-name">xxxx</h3>
										<div class="product-cards-detail-meta">
											<p class="product-cards-detail-price">
												0,000X <span>(xx)</span>
											</p>
											<p class="product-cards-detail-summary">
												xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx.
											</p>
										</div>
									</div>
								</a>
							</li>
							<li class="l-grid-col product-cards-item">
								<a class="product-cards-anc" href="detail.php">
									<div class="product-cards-portrait">
										<div class="product-cards-portrait-inner">
											<img src="img/product-1.jpg" />
										</div>
									</div>
									<div class="product-cards-detail">
										<h3 class="product-cards-name">xxxx</h3>
										<div class="product-cards-detail-meta">
											<p class="product-cards-detail-price">
												0,000X <span>(xx)</span>
											</p>
											<p class="product-cards-detail-summary">
												xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx.
											</p>
										</div>
									</div>
								</a>
							</li>
							<li class="l-grid-col product-cards-item">
								<a class="product-cards-anc" href="detail.php">
									<div class="product-cards-portrait">
										<div class="product-cards-portrait-inner">
											<img src="img/product-1.jpg" />
										</div>
									</div>
									<div class="product-cards-detail">
										<h3 class="product-cards-name">xxxx</h3>
										<div class="product-cards-detail-meta">
											<p class="product-cards-detail-price">
												0,000X <span>(xx)</span>
											</p>
											<p class="product-cards-detail-summary">
												xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx.
											</p>
										</div>
									</div>
								</a>
							</li>
							<li class="l-grid-col product-cards-item">
								<a class="product-cards-anc" href="detail.php">
									<div class="product-cards-portrait">
										<div class="product-cards-portrait-inner">
											<img src="img/product-1.jpg" />
										</div>
									</div>
									<div class="product-cards-detail">
										<h3 class="product-cards-name">xxxx</h3>
										<div class="product-cards-detail-meta">
											<p class="product-cards-detail-price">
												0,000X <span>(xx)</span>
											</p>
											<p class="product-cards-detail-summary">
												xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx.
											</p>
										</div>
									</div>
								</a>
							</li>
							<li class="l-grid-col product-cards-item">
								<a class="product-cards-anc" href="detail.php">
									<div class="product-cards-portrait">
										<div class="product-cards-portrait-inner">
											<img src="img/product-1.jpg" />
										</div>
									</div>
									<div class="product-cards-detail">
										<h3 class="product-cards-name">xxxx</h3>
										<div class="product-cards-detail-meta">
											<p class="product-cards-detail-price">
												0,000X <span>(xx)</span>
											</p>
											<p class="product-cards-detail-summary">
												xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx.
											</p>
										</div>
									</div>
								</a>
							</li>
						</ul>


						<nav class="product-cards-paging">
							<ul>
								<li>
									<a href="#">&lt;</a>
								</li>
								<li>
									<a href="#">1</a>
								</li>
								<li>
									<a class="is-active" href="#">2</a>
								</li>
								<li>
									<a href="#">3</a>
								</li>
								<li>
									<a href="#">4</a>
								</li>
								<li>
									<a href="#">5</a>
								</li>
								<li>
									<a href="#">&gt;</a>
								</li>
							</ul>
						</nav>


					</div>


					

				</div>








			</div>
		</div>
	</main>
<?php 
	include 'footer.php';
?>